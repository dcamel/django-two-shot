#Feature 7: adding redirect
from django.shortcuts import render, redirect
#Feature 7: adding login
#Feature 9: adding logout
from django.contrib.auth import login, authenticate, logout
#Feature 10: adding User for signup
from django.contrib.auth.models import User
#Feature 7: adding loginform
#Feature 10: adding signupform
from accounts.forms import LoginForm, SignUpForm


# Create your views here.

#Feature 7: adding Login
def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
    #This is technically a "GET" since its opposite of
        form = LoginForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


#Feature 9: adding Logout
def user_logout(request):
    logout(request)
    return redirect("login")


#Feature 10: adding signup
def user_signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']

            if password == password_confirmation:
                #Create a new user with those values
                user = User.objects.create_user(
                    username,
                    password=password,
                )

                #login the user
                login(request, user)
                return redirect("home")

            else:
                form.add_error("password", "Passwords do not match")
    else:
        form = SignUpForm()
    context = {
        "form":form,
    }
    return render(request, "accounts/signup.html", context)
