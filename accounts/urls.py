# Feature 7: creating
from django.urls import path
# Feature 7: adding user_login
# Feature 9: adding user_logout
# Feature 10: adding user_signup
from accounts.views import user_login, user_logout, user_signup

#make sure you don't define functions called "signup", "login", and "logout"
#they are already in use by django and will mess up


urlpatterns = [
    #Feature 7: adding login
    path("login/", user_login, name="login"),
    #Feature 9: adding logout
    path("logout/", user_logout, name="logout"),
    #Feature 10: adding signup
    path("signup/", user_signup, name="signup"),

]

    #note to use user_name for these.  Not doing so messes things up.
#    path("recipes/", recipe_list, name="recipe_list"),


# When a user visits example.com/signup,
# Django will invoke "signup" view function to handle the request,
# and this pattern is refered to as "signup" for links or redirects within the django project
