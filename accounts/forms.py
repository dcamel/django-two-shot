#Feature 7: creating
from django import forms


#Feature 7: adding Login
class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )
    # password_confirmation = forms.CharField(
    #     max_length=150,
    #     widget=forms.PasswordInput,
    # )

#Feature 10: adding Signup
class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
        #widget overrides <input type="text">
    )
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )
