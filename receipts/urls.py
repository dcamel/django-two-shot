# Feature 5: Creating file
from django.urls import path
# Feature 5: adding Receipt_List
# Feature 11: adding create_receipt
# Feature 12: adding category_list and account_list
# Feature 13: adding create_category
# Feature 14: adding create_account
from receipts.views import receipt_list, create_receipt, category_list, account_list, create_category, create_account
# Feature 7:  Trying this
from .views import receipt_list

urlpatterns = [
    # Feature 5: adding first views
    path("", receipt_list, name="home"),
    # Feature 11: adding create_receipt
    path("create/", create_receipt, name="create_receipt"),
    # Feature 12: adding category_list and account_list
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    # Feature 13: adding create_category
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]

# http://localhost:8000/receipts is our base for all addys right now
# http://localhost:8000/receipts/create/
# http://localhost:8000/receipts/categories/
# http://localhost:8000/receipts/categories/create/

#    path("create/", create_receipt, name="create_receipt"),
#    path("categories/create/", create_category, name="create_category"),
#
#   Notice that create_receipt path doesn't have "receipts/create/"
#   In views.py, we specify _where_ the files are.
#   Urls just handles what the URLs will be called.
