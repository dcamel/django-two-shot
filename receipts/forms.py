# Feature 11: creating this forms.py
from django.forms import ModelForm
# Feature 11: adding Receipt
# Feature 13: adding ExpenseCategory
# Feature 14: adding Account
from receipts.models import Receipt, ExpenseCategory, Account

#Feature 11: adding ReceiptForm
class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        #this means create a new Receipt in the receipts.model
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]

# Feature 13: adding CategoryForm
class CategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]

# Feature 14: adding AccountForm
class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]
