from django.db import models
#Feature 3: adding for django user account use
from django.conf import settings

# Create your models here.



#Feature 3: added TodoList
#The ExpenseCategory model is a value that we can apply to receipts like "gas" or "entertainment".
class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        #this will relate to accounts app
        settings.AUTH_USER_MODEL,
        related_name="categories",
        on_delete=models.CASCADE,
    )

#The Account model is the way that we paid for it, such as with a specific credit card or a bank account.
class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name= "accounts",
        on_delete=models.CASCADE,
    )

#The Receipt model is the primary thing that this application keeps track of for accounting purposes.
    #a date property that contains a date and time of when the transaction took place
class Receipt(models.Model):
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(max_digits=10, decimal_places=3)
    tax = models.DecimalField(max_digits=10, decimal_places=3)
    date = models.DateTimeField()
    purchaser = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name= "receipts",
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        ExpenseCategory,
        related_name= "receipts",
        on_delete=models.CASCADE,
    )
    account = models.ForeignKey(
        Account,
        related_name= "receipts",
        on_delete=models.CASCADE,
        null= True,
    )
