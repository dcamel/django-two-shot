# Feature 11: adding redirect
from django.shortcuts import render, redirect
# Feature 12: adding ExpenseCategory and Account
from receipts.models import Receipt, ExpenseCategory, Account
# Feature 8: protecting list view
from django.contrib.auth.decorators import login_required
# Feature 11: adding ReceiptForm
# Feature 13: adding CategoryForm
# Feature 14: adding AccountForm
from receipts.forms import ReceiptForm, CategoryForm, AccountForm

# Create your views here.

# Feature 8: protecting the list-view, and changing objects.filter
    #adding this one line will require the login
    #instructions wanted "purchaser" field used
@login_required
def receipt_list(request):
#   receiptlists = Receipt.objects.all()
    receiptlists = Receipt.objects.filter(purchaser=request.user)
    context = {
        "ReceiptList": receiptlists,
    }
    return render(request, "receipts/list.html", context)
    #context means able to search information, right?

# Feature 11: adding create_receipt
# specified 'purchaser' be set to current user
@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


# Feature 12: adding category_list
# note we are using the filter of "owner", as its a list of the "owners" entries
@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "CategoryList": categories,
    }
    return render(request, "categories/list.html", context)


# Feature 12: adding account_list
# note we are using the filter of "owner", as its a list of the "owners" entries
@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "AccountList": accounts
    }
    return render(request, "accounts/list.html", context)


#Feature 13: adding create_category
#on the creates, "receipt, category, account" didn't exist.  We used w/e
@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CategoryForm()
    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)


#Feature 14: adding create_account
@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/create.html", context)
