from django.contrib import admin
#Feature 4: Adding our first 3 models
from receipts.models import ExpenseCategory, Account, Receipt

# Register your models here.

#Feature4: Added our three models
@admin.register(ExpenseCategory)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "owner",
    ]

@admin.register(Account)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "number",
        "owner",
    ]

@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = [
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
    ]
